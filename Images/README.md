# Изображения

`<img src="images/cat.png" alt="" title="" />`

## Форматы:

| Формат | Кол-во цветов | Прозрачность | Растр/Вектор | Анимация |
|---|:---:|:---:|:---:|:---:|
| `png-24` | ~16 млн. | + (256) град. | Растр | - |
| `png-8` | 256 | + | Растр | - |
| `gif` | 256 | + | Растр | + |
| `jpg` | ~16 млн. | - | Растр | - |
| `svg` | ~16 млн. | + | Вектор | + (программно) |
| `webp` | ~16 млн. | + (256) | Растр | + |

## Оптимизация:
[Tiny PNG](https://tinypng.com/)

[OptimiZilla](https://imagecompressor.com/ru/)

[Compressor](https://compressor.io/)
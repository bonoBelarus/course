# Шрифты

## Подключение [Google Fonts](https://fonts.google.com/) шрифтов:
 `<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap&subset=cyrillic" rel="stylesheet">`
 ___

## Подключение локальных шрифтов
```
@font-face {
  font-family: "Roboto";
  font-style: normal;
  font-weight: 300;
  src: url("../fonts/Roboto/Roboto-Light.ttf") format('truetype');
}
```
```
@font-face {
  font-family: "Roboto";
  font-style: normal;
  font-weight: 400;
  src: url("../fonts/Roboto/Roboto-Regular.ttf") format('truetype');
}
```
```
@font-face {
  font-family: "Roboto";
  font-style: normal;
  font-weight: 700;
  src: url("../fonts/Roboto/Roboto-Bold.ttf") format('truetype');
}
```
___
## Синтаксис

`font: style weight size/line-height family;`

`font: normal 100 16px/1.2 'Roboto Mono', sans-serif;`

* `font-style` - italic
* `font-weight` - 300/400/500/...
* `font-size` - 16px/1.5rem/1.5em
* `line-height` - 16px/1.5rem/1.5em
* `font-family` - Arial, sans-serif

# Фон

## `background: #000 url(../images/cat.png) 50% 100% no-repeat;`
* `background-color: #000;`
* `background-image: url(../images/cat.png);`
* `background-image: linear-gradient(45deg, #000 5%, red, #000 95%);`
    * `radial-gradient(circle at 50%, #000 5%, red, #000 95%);`
    * `radial-gradient(ellipse at 50%, #000 5%, red, #000 95%);`
* `background-repeat: no-repeat;` repeat
* `background-repeat-x: no-repeat;` repeat
* `background-repeat-y: no-repeat;` repeat
* `background-position: 50% 100%;` center bottom | center
___
## `background-size`

* `auto` — по-умолчанию. исходный размер изображения.
* `cover` — заполняет собой весь контейнер.
* `contain` — старается полностью поместить в контейнер изображение.
Не обрезая его.
* `100%` — величина относительно контейнера.
* `100px` — абсолютная величина в пикселях.
___
## `background-attachment`

Определяет, является ли позиция изображения фиксированной в области просмотра, или прокручивается вместе с содержащим его блоком.

* `fixed` — фон фиксирован в области просмотра.
* `scroll` — фон прокручивается вместе с содержащим его блоком.

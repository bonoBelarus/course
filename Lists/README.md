# Списки

* `<ul>` — Unordered List
* `<ol>` — Ordered List
* `<dl>` — Definition List

Элементы списка:

* `<li>` — List Item
* `<dt>` — Definition Title
* `<dd>` — Definition Details

`<NB!>:` Внутри `<ul>` и `<ol>` могут находиться только `<li>`

## Примеры:

### Маркированный:

    <ul>
      <li>Яблоко</li>
      <li>Груша</li>
      <li>Ананас</li>
      <li>Банан</li>
      <li>Арбуз</li>
    </ul>

* Яблоко
* Груша
* Ананас
* Банан
* Арбуз


### Нумерованный:

    <ol>
      <li>Кот</li>
      <li>Собака</li>
      <li>Гусь</li>
      <li>Улитка</li>
      <li>Лев</li>
    </ol>

1. Кот
1. Собака
1. Гусь
1. Улитка
1. Лев


### Нумерованный:

    <dl>
      <dt>Режиссер:</dt>
      <dd>Петр Точилин</dd>
      <dt>В ролях:</dt>
      <dd>Андрей Гайдулян</dd>
      <dd>Алексей Гаврилов</dd>
      <dd>Виталий Гогунский</dd>
      <dd>Мария Кожевникова</dd>
    </dl>
  <dl>
    <dt>Режиссер:</dt>
    <dd>Петр Точилин</dd>
    <dt>В ролях:</dt>
    <dd>Андрей Гайдулян</dd>
    <dd>Алексей Гаврилов</dd>
    <dd>Виталий Гогунский</dd>
    <dd>Мария Кожевникова</dd>
  </dl>
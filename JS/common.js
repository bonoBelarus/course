const arr = [
  { id: 'ID_NAME_1', payload: { x: 1, y: 2, z: 3 }},
  { id: 'ID_NAME_2', payload: { x: 4, y: 5, z: 6 }}
]

const obj = arr.reduce((a, b) => {
  // console.log(a)
  a[b.id] = b.payload
  return a
}, {})

console.log(obj["ID_NAME_1"])

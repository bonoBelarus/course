# Ссылки

`<a href="/about.html" title="О нас">О нас</a>`

Относительный путь | Относительно корня сайта | Абсолютный путь |
|---|---|--|
| `href="about.html"` | `href="/about.html"` | `href="http://example-site.com/about.html"` |
| `href="./about.html"` | `href="/about.html"` | `href="http://example-site.com/about.html"` |
| `href="pages/faq.html"` | `href="/pages/faq.html"` | `href="http://example-site.com/pages/faq.html"` |
| `href="../catalog.html"` | `href="/about.html"` | `href="http://example-site.com/catalog.html"` |

Атрибут `target` со значением `_blank` даёт возможность открыть ссылку в новой вкладке.

Хорошей практикой является добавление атрибута `title`.